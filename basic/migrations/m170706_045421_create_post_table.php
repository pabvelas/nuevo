<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170706_045421_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            "title"=> $this->string()->notNull(),
            "content"=>$this->text()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
